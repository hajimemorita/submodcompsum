
require "jcode" 
require 'getopts'
require 'rexml/document'
include REXML

if !getopts("help", "e:", "t:", "a:", "b:", "idf", "crf","hr", "tf:", "df:", "ds:") || !(($OPT_e & $OPT_t) | $OPT_a) || ($OPT_e ^ $OPT_t )
  $stderr.puts("illegal option")
  print_usage
  exit(2)
end
 
file = File.new($OPT_a.to_s)
doc  = Document.new(file.read)# REXML doesn't see KCODE

entry_list = Hash.new(false)
topic_list = Array.new 

IO.popen('~/local/bin/juman', 'r+') do |io|
  doc.root.each_element('//*[./ANSWERTYPE[not(text()="DATE" or text()="ORGANIZATION" or text()="PERSON" or text()="LOCATION")]]'){|topic| #{{{
    id    = topic.attribute("ID").to_s 
    id =~ /.*?(\d+)$/ 
    number = $1 
    title     = (topic.attribute("TITLE").to_s) 
    narrative = (topic.get_elements("NARRATIVE[@LANG='JA']")[0].text)
    nuggets =  (topic.get_elements("ANSWER/NUGGET"))

    puts "Qid 1.#{number} :#{narrative}"
    counter = 1 
    nuggets.each{|nug|
      index = nug.attribute("ID").to_s 
      #puts nug.attribute("VITAL").to_s 
      vital =  nug.attribute("VITAL").to_s.to_i 
      #puts nug.attribute("NONVITAL").to_s 
      nonvital = nug.attribute("NONVITAL").to_s.to_i 
      io.puts nug.text
      rstring = "" 
        
      loop{
        result = io.gets 
        #puts result 
        #p result 
        break if result=~/EOS/ 
        result.each{|line| 
          next if line =~ /^@/
          rstring += line.split(/\s+/)[2] 
          rstring += " "
        }
      } 
      #puts nug.text #juman通す

      (vital).times{
        puts "1.#{number} #{counter} vital #{rstring}"
        counter +=1 
      }
      
      (nonvital).times{
        puts "1.#{number} #{counter} okay #{rstring}"
        counter +=1 
      } 
    } 
    puts 
  }#}}}
end 
