class Hash#{{{
  def each_pair(hash, &block) 
    return nil if block.arity != 2 
    self.keys.each do |key1|
      hash.keys.each do |key2| 
        yield(key1,key2) 
      end 
    end 
  end 
end#}}}
class Array#{{{
  def each_other(&block)
    arity = block.arity
    return self.each(&block) if arity != 2
      
    (0...self.size).each{|index1| 
      (index1+1...self.size).each{|index2| 
        yield(self[index1],self[index2]) 
      } 
    } 
    self
  end
  def each_other_with_index(&block)
    arity = block.arity
    return self.each(&block) if arity != 4
      
    (0...self.size).each{|index1| 
      (index1+1...self.size).each{|index2| 
        yield(self[index1],index1,self[index2],index2) 
      } 
    } 
    self
  end
  def each_pair(ary, &block) 
    return nil if block.arity != 2 
    self.each do |key1|
      ary.each do |key2| 
        yield(key1,key2) 
      end 
    end 
  end 
  def collect_pair(ary, &block) 
    return nil if block.arity != 2 
    ret = Array.new 
    self.each do |key1|
      ary.each do |key2| 
        ret << yield(key1,key2) 
      end 
    end 
    ret 
  end 
  def each_with_index(&block) 
    return nil if block.arity != 2
    self.each_index do |ind| 
      yield( self[ind], ind) 
    end 
  end 
end#}}}
