# make_subtree_extractive_summary.rb
ruby make_subtree_extractive_summary.rb -q query_and_source_document_file

# inputfile query_and_source_document
raw output of KNP.   
The script recongnize first sentence as a query sentence.

# case_analysis.c.patch
a patch for knp 4.01 to output obligatory link. Patch this file to case_analysis.c in knp. (The patch can be too old for today's KNP. But it just add 2 lines to KNP. The patch may provide some useful information, when you modify newer KNP.)

The patch will slightly change output of KNP.
```  
ニ/C/真希/8/0/2; => @*ニ/C/真希/8/0/2;  
```
("@" measns 真希に(8th bunsetsu) is obligatory case for the bunsetsu.
"*" means the case is adjacency case.)

The example is as follows.  

```
...  
 + 9D <人名><ニ><助詞><体言><係:ニ格><区切:0-0><格要素><連用要素><名詞項候補><先行詞候補><正規化代表表記:真希/まき><解析連格:ガ><解析格:ニ>  
真希 まき 真希 名詞 6 人名 5 * 0 * 0 "人名:日本:名:877:0.00019 疑似代表表記 代表表記:真希/まき" <人名:日本:名:877:0.00019><疑似代表表記><代表表記:真希/まき><正規化代表表記:真希/まき><漢字><かな漢字><名詞相当語><自立><複合←><内容語><タグ単位始><固有キー><文節主辞>  
さん さん さん 接尾辞 14 名詞性名詞接尾辞 2 * 0 * 0 "代表表記:さん/さん" <代表表記:さん/さん><正規化代表表記:さん/さん><呼掛><かな漢字><ひらがな><名詞相当語><付属>  
に に に 助詞 9 格助詞 1 * 0 * 0 NIL <かな漢字><ひらがな><付属>  
* -1D <BGH:出会う/であう><文末><時制-過去><句点><用言:動><レベル:C><区切:5-5><ID:（文末）><係:文末><提題受:30><主節><格要素><連用要素><動態述語><正規化代表表記:出会う/であう><主辞代表表記:出
会う/であう>  
+ -1D <BGH:出会う/であう><文末><時制-過去><句点><用言:動><レベル:C><区切:5-5><ID:（文末）><係:文末><提題受:30><主節><格要素><連用要素><動態述語><正規化代表表記:出会う/であう><用言代表表記:出
>会う/であう><主題格:一人称優位><格関係3:デ:休耕田><格関係8:ニ:真希>< 格解析結果:出会う/であう:動1:@ガ/U/-/-/-/-;ヲ/U/-/-/-/-;@*ニ/C/真希/8/0/2;ト/U/-/-/-/-;@デ/C/休耕田/3/0/2;カラ/U/-/-/-/-;ヨリ/U/-/-/-/-;マデ/U/-/-/-/-;ヘ/U/-/-/-/-;@時間/U/-/-/-/-;@外の関係/U/-/-/-/-;@ノ/U/-/-/-/-;@修飾/U/-/-/-/-;ヲツウジル/U/-/-/-/-;トスル/U/-/-/-/-;ニヨル/U/-/-/-/-;ヲハジメル/U/-/-/-/-;ニオク/U/-/-/-/-;ニカギル/U/-/-/-/-;ニトル/U/-/-/-/->  
出会った であった 出会う 動詞 2 * 0 子音動詞ワ行 12 タ形 10 "代表表記:出会う/であう" <代表表記:出会う/であう><正規化代表表記:出会う/であう><表現文末><かな漢字><活用語><自立><内容語><タグ単位
始><文節始><文節主辞>
```