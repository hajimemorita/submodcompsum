class KNPTree#{{{
    class Chunk#{{{
        attr_accessor :link
        attr_accessor :rel
        attr_accessor :closest
        attr_accessor :head# head?
        attr_accessor :func
        attr_accessor :score
        attr_accessor :tokens 
        attr_accessor :distance
        attr_accessor :essencial 

        def initialize link,rel#{{{
            @distance=Hash.new{|hash,key|hash[key]=1.0/0.0}
            self.link = link.to_i
            self.rel  = rel.to_s
            self.essencial = [] 
            #      self.head = head.to_i
            #      self.func = func.to_i
            #      self.score = score.to_f
            self.tokens = []
            if self.link != -1
                @distance[self.link] = 1
            end
        end   #}}}
    end#}}}
    class Token#{{{
        attr_accessor :surface
        attr_accessor :read
        attr_accessor :base
        attr_accessor :pos
        attr_accessor :link 
        attr_accessor :cform 
        attr_accessor :ctype 
        attr_accessor :ne
        def initialize surface,read,base,pos,cform,ctype,ne#{{{
            self.surface = surface
            self.read    = read   
            self.base    = base   
            self.pos     = pos    
            self.cform   = cform  
            self.ctype   = ctype  
            self.ne      = ne      
        end #}}}
    end #}}}
    attr_accessor :chunks
    attr_accessor :surface
    attr_accessor :head_chunk #it will made by initialize
    attr_accessor :head       #it will made by depdist
    attr_accessor :essencial_chunk_pair#When summary contains one chunk of the pair, summary must constains other one.
    attr_accessor :links      # 
    def words #{{{
        self.chunks.collect{|ch| ch.tokens.collect{ |tk| tk.base}}.flatten
    end #}}}
    def chunked_words #{{{
        self.chunks.collect{|ch| ch.tokens.collect{|tk| [tk.surface, tk.pos]}}
    end #}}}
    def chunked_words_full #{{{
        self.chunks.collect{|ch| ch.tokens.collect{|tk| [tk.surface, tk.pos, tk.base]}}
    end #}}}
    def jiritsu #{{{
        jiritsu = [] 
        self.chunks.each{|ch| 
            ch.tokens.each{|tk| 
                tokenpos = (tk.pos) 
                next if !(tokenpos=~/^名詞/||tokenpos=~/^動詞/||tokenpos=~/^形容/||tokenpos=~/^副詞/) 
                jiritsu << (tk.base) 
            }  
        }    
        return jiritsu 
    end #}}}
    def jdepdist #jiritsu 用{{{ 
        distance = Hash.new{|hash,key|hash[key]=1.0/0.0}
        jiritsu  = @jiritsu_dist

        jiritsu.collect!{|jr|
            dephash= Hash.new{|hash,key|hash[key]=1.0/0.0}
            jr[1].each{|i,j| #i=行き先 j=距離
                @jindex[i].each{|ci| dephash[ci] = j}
            } #チャンクへの距離から単語への距離へ   
            #p [jr[0], dephash] 
            [jr[0], dephash.dup] 
        }    
        #p jiritsu 
        jiritsu.each_index{|i|
            jiritsu.each_index{|j| 
                key = [jiritsu[i][0], jiritsu[j][0]]
                begin 
                    raise if ( jiritsu[i][1][j].to_f.finite?  && jiritsu[i][1][j] != jiritsu[j][1][i] )
                    distance[key] = [distance[key], jiritsu[i][1][j], jiritsu[j][1][i]].min
                    #distance[key] = -1 if distance[key].to_f.infinite?
                    raise if distance[key].to_f.infinite?
                rescue
                    puts "err"
                    p distance[key].to_f
                    p distance
                    jiritsu.each{|j|
                        print "#{j[0]}"  
                        p j[1]
                    }
                    p @chunk_dist 
                    p [i,j] 
                    p jiritsu[i][1]#[j] 
                    p jiritsu[j][1]#[i] 
                    puts surface.join("") 
                    puts "#{distance[key]}, #{key}, #{jiritsu[i][1].size}, #{jiritsu[j][1].size}"
                    raise 
                    exit 
                end 
            }  
        }    
        #p distance 
        return distance 
    end #}}} 
    def wdepdist #word 用{{{ 
        distance = Hash.new{|hash,key|hash[key]=1.0/0.0}
        words  = @word_dist

        words.collect!{|jr|
            dephash= Hash.new{|hash,key|hash[key]=1.0/0.0}
            jr[1].each{|i,j| #i=行き先 j=距離
                @windex[i].each{|ci| dephash[ci] = j}
            } #チャンクへの距離から単語への距離へ   
            #p [jr[0], dephash] 
            [jr[0], dephash.dup] 
        }    
        #p jiritsu 
        words.each_index{|i|
            words.each_index{|j| 
                key = [words[i][0], words[j][0]] 
                begin 
                    #raise if ( jiritsu[i][1][j].to_f.finite?  && jiritsu[i][1][j] != jiritsu[j][1][i] )
                    distance[key] = [distance[key], words[i][1][j], words[j][1][i]].min
                    #distance[key] = -1 if distance[key].to_f.infinite? 
                    raise if distance[key].to_f.infinite? 
                rescue 
                    puts "err" 
                    p distance[key].to_f
                    p distance 
                    p jiritsu 
                    p [i,j] 
                    p jiritsu[i][1]#[j] 
                    p jiritsu[j][1]#[i] 
                    puts "#{distance[key]}, #{key}, #{jiritsu[i][1].size}, #{jiritsu[j][1].size}"
                    exit 
                end 
            }  
        }    
        #p distance 
        return distance 
    end #}}} 
    def cdepdist #chunk 用{{{ 
        distance = Hash.new{|hash,key|hash[key]=1.0/0.0}
        chunks  = @chunk_dist

        chunks.each_index{|i|
            chunks.each_index{|j| 
                key = [chunks[i][0], chunks[j][0]] 
                begin 
                    #raise if ( jiritsu[i][1][j].to_f.finite?  && jiritsu[i][1][j] != jiritsu[j][1][i] )
                    distance[key] = [distance[key], chunks[i][1][j], chunks[j][1][i]].min
                    #distance[key] = -1 if distance[key].to_f.infinite? 
                    raise if distance[key].to_f.infinite? 
                rescue 
                    puts "err" 
                    p distance[key].to_f
                    p distance 
                    p jiritsu 
                    p [i,j] 
                    p jiritsu[i][1]#[j] 
                    p jiritsu[j][1]#[i] 
                    puts "#{distance[key]}, #{key}, #{chunks[i][1].size}, #{chunks[j][1].size}"
                    exit 
                end 
            }    
        }      
        #p distance 
        return distance 
    end #}}} 
    def initialize parsetree #{{{
        #parsetree
        #[[*,+,w[tab separated],w,+,w,]#chank, [*,+,w]#chank,[]] #+は主節のみでも。。
        @windex = Hash.new{|hash,key| hash[key]=Array.new}
        @jindex = Hash.new{|hash,key| hash[key]=Array.new}
        tkindex = Hash.new{|hash,key| hash[key]=-1} 

        @jiritsu_dist = []
        @word_dist = []
        @chunk_dist = []
        @bases = []
        @links = []
        @essencial_chunk_pair = Hash.new{|hash,key| hash[key]=Array.new}
        self.chunks = []
        @surface = []
        @scindex = Hash.new 
        sccount = 0 
        @head = nil

        (0...parsetree.size).each do |i| #bunsetsu
            #     # S-ID:1 KNP:3.1- DATE:2011/12/21 SCORE:-80.15983
            #     * 11D <SM-主体><SM-場所><SM-組織><BGH:アカデミー/あかでみー><文頭><ハ><助詞><体言><係:未格><提題><区切:3-5><主題表現><格要素><連用要素><正規化代表表記:スウェーデン/すうぇーでん+王立/おうりつ+科学/かがく+アカデミー/あかでみー><主辞代表表記:アカデミー/あかでみー>
            #     + 1D <文節内><係:文節内><文頭><地名><体言><名詞項候補><先行詞候補><正規化代表表記:スウェーデン/すうぇーでん>
            #スウェーデン すうぇーでん スウェーデン 名詞 6 地名 4 * 0 * 0 "代表表記:スウェーデン/すうぇーでん 地名:国" <代表表記:スウェーデン/すうぇーでん><地名:国><正規化代表表記:スウェーデン/すうぇーでん><文頭><記英数カ><カタカナ><名詞相当語><自立><内容語><タグ単位始><文節始><固有キー>
            #     + 2D <BGH:王立/おうりつ><文節内><係:文節内><体言><名詞項候補><先行詞候補><正規化代表表記:王立/おうりつ>
            #王立 おうりつ 王立 名詞 6 普通名詞 1 * 0 * 0 "代表表記:王立/おうりつ カテゴリ:抽象物 ドメイン:政治" <代表表記:王立/おうりつ><カテゴリ:抽象物><ドメイン:政治><正規化代表表記:王立/おうりつ><漢字><かな漢字><名詞相当語><自立><複合←><内容語><タグ単位始>
            #     + 3D <BGH:科学/かがく><文節内><係:文節内><体言><名詞項候補><先行詞候補><正規化代表表記:科学/かがく>
            #     科学 かがく 科学 名詞 6 普通名詞 1 * 0 * 0 "代表表記:科学/かがく カテゴリ:抽象物 ドメイン:科学・技術" <代表表記:科学/かがく><カテゴリ:抽象物><ドメイン:科学・技術><正規化代表表記:科学/かがく><漢字><かな漢字><名詞相当語><自立><複合←><内容語><タグ単位始>
            chunk = parsetree[i]

            #link
            #p chunk.size() 
            #p chunk
            #stop 
            chunk[0] =~ /^\* ((?:-|\d)+)(\w)\s/
                link = ($1.to_i)
            rel  = $2.to_s
            #chunk[0] =~ /<BGH:(.*?)>/
            #head = $1
            chunk_id = self.chunks.size
            #puts "* #{chunk_id} #{link}, #{rel} "
            #p chunk.size() 
            # 
            new_chunk = Chunk.new(link, rel)
            #calculate distance between itself and parent node
            self.chunks.each{|ck|
                if(ck.distance.has_key?(chunk_id) && ck.distance[chunk_id] == 1)
                    ck.distance.each{|id,dist|#id??
                        #puts "#{ck.tokens.inject(""){|sum,n| sum+n.surface}} #{id}"
                        new_chunk.distance[id] = dist+1
                        self.chunks[id].distance[chunk_id] = [self.chunks[id].distance[chunk_id], new_chunk.distance[id]].min if id < chunk_id
                    }  
                end  
            }      
            new_chunk.distance[chunk_id] = 0 
            if new_chunk.link != -1  
                new_chunk.distance[new_chunk.link] = 1 
            else
                @head_chunk = chunk_id    #chunk number
                #@head = @index[i] #まだわかんない
            end

            #new_chunk経由の距離を計算 
            self.chunks << new_chunk
            new_chunk.distance.each{|id,dist|
                next if id == new_chunk.link 
                new_chunk.distance.each{|id2,dist2|
                    self.chunks[id].distance[id2] = [dist+dist2, self.chunks[id].distance[id2]].min #if id < chunk_id
                }  
            }    
            #p new_chunk.distance 
            (1..chunk.size()-1).each{|j|
                #p chunk[i]
                #+ -1D <BGH:発表/はっぴょう+する/する><文末><補文ト><サ変動詞><時制-過去><句点><用言:動><レベル:C><区切:5-5><ID:（文末）><係:文末><提題受:30><主節><格要素><連用要素><動態述語><サ変><正規化代表表記:発表/はっぴょう><用言代表表記:発表/はっぴょう><主題格:一人称優位><格関係3:ガ:アカデミー><格関係30:ト:贈る><格解析結果:発表/はっぴょう:動1:@ガ/N/アカデミー/3/0/1;@ニ/U/-/-/-/-;@*ト/C/贈る/30/0/1;@デ/U/-/-/-/-;カラ/U/-/-/-/-;ヨリ/U/-/-/-/-;マデ/U/-/-/-/-;ヘ/U/-/-/-/-;@時間/U/-/-/-/-;@外の関係/U/-/-/-/-;修飾/U/-/-/-/->
                if(chunk[j] =~ /^\+.*/) 
                    #print "+ #{sccount}" 
                    @scindex[sccount] = self.chunks.size-1
                    sccount+=1 
                    if(chunk[j] =~ /^\+.*<格解析結果:([^>]*)>.*/) 
                        #chunkに必須の係り受けについて情報を書き込む 
                        result = $1   
                        #<格解析結果:贈る/おくる:動1:@ガ/U/-/-/-/-;@*ヲ/C/賞/9/0/1;@ニ/C/３/29/0/1;@ト/U/-/-/-/-;@デ/U/-/-/-/-;@カラ/U/-/-/-/-;ヨリ/U/-/-/-/-;マデ/U/-/-/-/-;@ヘ/U/-/-/-/-;@時間/U/-/-/-/-;@外の関係/U/-/-/-/-;@ノ/U/-/-/-/-;@修飾/U/-/-/-/-;トスル/U/-/-/-/-;ニタイスル/U/-/-/-/-;ニムケル/U/-/-/-/-;ヲツウジル/U/-/-/-/-;ニヨル/U/-/-/-/-;ニアワセル/U/-/-/-/-;ニツク/U/-/-/-/-> 
                        rscan = result.scan(/[:;]?(\@?)(\*?)([^:\/]*?)\/([^\/]*?)\/([^\/]*?)\/([^\/]*?)\/([^\/]*?)\/([^\/;]*?)(?:;|$)/)
                        #p rscan
                        rscan.each{|adj,need,cf,type,wd,index,e,e2|
                            #print adj+need+wd+index+ "; "
                            if( (adj=="@" || need=="*")&&wd != "-" )
                                self.chunks[-1].essencial << [wd,index]
                            end 
                        }  
                        #print result
                        #gets 
                    end 
                    #puts 
                else #     科学 かがく 科学 名詞 6 普通名詞 1 * 0 * 0 "代表表記:科学/かがく カテゴリ:抽象物 ドメイン:科学・技術" <代表表記:科学/かがく><カテゴリ:抽象物><ドメイン:科学・技術><正規化代表表記:科学/かがく><漢字><かな漢字><名詞相当語><自立><複合←><内容語><タグ単位始>
                    #やること
                    #単語indexとchunk indexの対応
                    #token obj生成してchunkに登録
                    #共起ペアを登録、距離を計算？ 
                    tab = chunk[j].split(/\s+/)# =~ /^([^ ]+) /
                    surf = tab[0]
                    base = tab[2] 
                    pos  = tab[3]+tab[5] 
                    #tags = chunk[i].split(/[<>]+/)
                    #puts "  "+ surf+" "+base+" "+pos 

                    self.chunks[-1].tokens << Token.new(surf, tab[1], base, pos, "", "", "")
                    @surface << surf
                end 
            }    
        end    

        self.chunks.each_with_index{|ch,chunk_id|
            ch.essencial.each{|es|
                #puts "es  #{es[0]} #{self.chunks[@scindex[es[1].to_i]].tokens.inject(""){|sum,n|sum+n.surface}} #{ch.tokens.inject(""){|sum,n|sum+n.surface}}"
                @essencial_chunk_pair[chunk_id] << @scindex[es[1].to_i]
                @essencial_chunk_pair[@scindex[es[1].to_i]] << chunk_id
            } 
        }

        chunk_id = 0 
        link_chunk = [] 
        @links = [] 
        self.chunks.each{|ch| 
            token_words = []
            ch.tokens.each{|tk| 
                #tokenpos = $Ico_etu.iconv(tk.pos) 
                tokenpos  = tk.pos
                @windex[chunk_id] += [@word_dist.size] 
                @word_dist  << [tk.base, ch.distance.dup] 
                token_words << [tk.surface, tk.pos ]
                next if !(tokenpos=~/^名詞/||tokenpos=~/^動詞/||tokenpos=~/^形容/||tokenpos=~/^副詞/)
                @jindex[chunk_id] += [@jiritsu_dist.size] 
                @jiritsu_dist << [tk.base, ch.distance.dup]
                link_chunk << [chunk_id, ch.link]
            }    
            #やってもうた．． 
            if ch.link != -1 
                @links << [ch.link]
            else 
                @links << []
            end 
            @chunk_dist << [token_words, ch.distance.dup] #[wordindex, dist]
            chunk_id += 1 
        }      
        #puts @jiritsu_dist 
        #puts @chunk_dist 
        #    @chunk_dist.each{|wd|
        #      puts wd[0] 
        #      p wd[1] 
        #    } 
        @head = @jindex[@head_chunk] 
        nil  
    end#}}}
end #}}} 

# Sentence class
class Sentence#{{{
    attr_accessor :line_number 
    attr_accessor :text 
    attr_accessor :bases 
    attr_accessor :chunks 
    attr_accessor :chunksf
    attr_accessor :jiritsu #[base,]
    attr_accessor :jiritsu_surface #[surface,]
    attr_accessor :wordsize 
    attr_accessor :distance_matrix_j
    attr_accessor :distance_matrix_w
    attr_accessor :distance_matrix_c
    attr_accessor :links 
    attr_accessor :essencial_chunk_pair#When summary contains one chunk of the pair, summary must constains other one.
    @@jiritsu_dict = Hash.new 
    def initialize(text) # a sentence{{{
        @wordsize = 0.0 
        @essencial_chunk_pair = Hash.new{|hash,key| hash[key]=Array.new}
        @line_number = 1 
        @bases = [] 
        @text = text 

        # sample
        ## S-ID:1 KNP:3.1- DATE:2011/12/21 SCORE:-80.15983
        #* 11D <SM-主体><SM-場所><SM-組織><BGH:アカデミー/あかでみー><文頭><ハ><助詞><体言><係:未格><提題><区切:3-5><主題表現><格要素><連用要素><正規化代表表記:スウェーデン/すうぇーでん+王立/おうりつ+科学/かがく+アカデミー/あかでみー><主辞代表表記:アカデミー/あかでみー>
        #+ 1D <文節内><係:文節内><文頭><地名><体言><名詞項候補><先行詞候補><正規化代表表記:スウェーデン/すうぇーでん>
        # ーデン すうぇーでん スウェーデン 名詞 6 地名 4 * 0 * 0 "代表表記:スウェーデン/すうぇーでん 地名:国" <代表表記:スウェーデン/すうぇーでん><地名:国><正規化代表表記:スウェーデン/すうぇーでん><文頭><記英数カ><カタカナ><名詞相当語><自立><内容語><タグ単位始><文節始><固有キー>
        #+ 2D <BGH:王立/おうりつ><文節内><係:文節内><体言><名詞項候補><先行詞候補><正規化代表表記:王立/おうりつ>
        #おうりつ 王立 名詞 6 普通名詞 1 * 0 * 0 "代表表記:王立/おうりつ カテゴリ:抽象物 ドメイン:政治" <代表表記:王立/おうりつ><カテゴリ:抽象物><ドメイン:政治><正規化代表表記:王立/おうりつ><漢字><かな漢字><名詞相当語><自立><複合←><内容語><タグ単位始>
        #+ 3D <BGH:科学/かがく><文節内><係:文節内><体言><名詞項候補><先行詞候補><正規化代表表記:科学/かがく>
        #科学 かがく 科学 名詞 6 普通名詞 1 * 0 * 0 "代表表記:科学/かがく カテゴリ:抽象物 ドメイン:科学・技術" <代表表記:科学/かがく><カテゴリ:抽象物><ドメイン:科学・技術><正規化代表表記:科学/かがく><漢字><かな漢字><名詞相当語><自立><複合←><内容語><タグ単位始>

        # transform to array
        # [[*+..+..][*+..]]
        parsed = []
        text.split(/\n/).each{|line|
            if(line =~ /^\*/)
                parsed << [ line]
            elsif(line =~ /^\+/)
                parsed[-1] << line
            elsif(line =~ /^\#/)
                nil
            else# a word
                parsed[-1] << line
            end
        }  
        @tree ||= KNPTree.new(parsed)
        @jiritsu = @tree.jiritsu.collect{|word,dist| word}
        @words = @tree.words  
        @chunks = @tree.chunked_words 
        @chunksf = @tree.chunked_words_full 
        @essencial_chunk_pair = @tree.essencial_chunk_pair 

        @treej = @tree.jiritsu
        @bases = @tree.words
        @head  = @tree.head
        @links = @tree.links

        @distance_matrix_j = Array.new(@jiritsu.size){|i| Array.new(@jiritsu.size,-1)}
        @distance_matrix_w = Array.new(@words.size){|i| Array.new(@words.size,-1)}
        @distance_matrix_c = Array.new(@chunks.size){|i| Array.new(@chunks.size,-1)}
        jdepdlist = @tree.jdepdist 
        wdepdlist = @tree.wdepdist 
        cdepdlist = @tree.cdepdist 

        @treej.each_index{|i| @treej.each_index{|j|
            @distance_matrix_j[i][j] = jdepdlist[[@treej[i],@treej[j]]]
            if @distance_matrix_j[i][j].to_f.infinite?
                puts "j" 
                puts @distance_matrix_j[i][j] 
                exit 
            end 
        }}    
        @words.each_index{|i| @words.each_index{|j| 
            @distance_matrix_w[i][j] = wdepdlist[[@words[i],@words[j]]]
            puts "w" if @distance_matrix_w[i][j].to_f.infinite?
            exit if @distance_matrix_w[i][j].to_f.infinite?
        }}     
        @chunks.each_index{|i| @chunks.each_index{|j| 
            @distance_matrix_c[i][j] = cdepdlist[[@chunks[i],@chunks[j]]]
            puts "c" if @distance_matrix_c[i][j].to_f.infinite?
            p cdepdlist if @distance_matrix_c[i][j].to_f.infinite?
            exit if @distance_matrix_c[i][j].to_f.infinite?
        }}     

        @tree = nil # delete
        self.text = text 
    end #}}}
    def to_s
        @chunksf.inject(""){|s,n| s+n.inject(""){|sn,nn|sn+nn[0]}}
    end

    # find maximal density subtree from subtree
    def get_md_subtree_es(ind_top, rscore, weight_dict, overlap_hash, overlap_score, threshold, sentid, max_weight, factor, used_words )#{{{
        dens_array = Array.new
        essencial_array = Array.new
        terms = Hash.new{|hash,key| hash[key]=nil}   
        
        # calculate own density
        cscore  = 0.0 
        cweight = 0 
        flags = Array.new(overlap_hash.size,false) 
        wordset = [] 
        chunksf[ind_top].each_with_index{|tk, tk_ind| 
            score_tmp = 0 
            score_tmp = rscore[tk[2]]**($OPT_r).to_f if ((tk[1]=~/^名詞/||tk[1]=~/^動詞/||tk[1]=~/^形容/||tk[1]=~/^副詞/) && (rscore[tk[2]]>0)) #自立語のみ
            # 文と単語の対をもっておいて すでに同じ文からある単語がカバーされていたら, ここでその単語のスコアを無効化？
            # 使った単語一覧にも含めない必要があるので. あとで使用単語をカウントするところを直さないといけないかも... 
            next if(used_words.include?(tk[2])) #スキップ 

            flags[overlap_hash[tk[2]]] = true if (overlap_hash.has_key?(tk[2]))
            if score_tmp == nil  
                STDERR.puts $Ico_etu.iconv(tk[2])
            else 
                wordset << tk[2] 
                cscore += score_tmp if score_tmp >= 0#0以上に制限
            end 
        }

        current_ind = ind_top 
        minset = [] 
        # 係り受けをたどれるところまで辿る
        # 自分を除く係り受けをたどったノード集合を求める 
        # follow dependencies until the leaf.
        # get a set of nodes except for itself following the dependencies.
        if($OPT_direct) #only use direct dependency
            minset << links[current_ind][0] if links[current_ind]!=[]#
        else 
            (next_ind = links[current_ind])  #自分のかかり先
            while( next_ind != [] && next_ind != nil )
                minset << next_ind[0]  
                current_ind = next_ind[0] 
                (next_ind = links[current_ind]) #次のかかり先
            end
        end 

        #それぞれとのペアを列挙. 一つ上からルートまでのペアと差集合をとって(おなじペアが重複する可能性があるから)スコアに足す.
        #かかり先ノードが含む単語集合
        cweight = weight_dict["w_#{sentid}_#{ind_top}"]
        cscore = 0 if(cweight == 0) 
        cscore += factor * cweight  if(cscore > 0)

        used_pairs = []
        terms[[cweight, flags]] = [cscore , [ind_top], used_pairs ] #係もとごとに重み＋密度ハッシュを用意 

        # かかり元を探す
        linkedlist = []
        essenciallist = []
        links.each_with_index{|x,i| 
            if(x[0] == ind_top.to_i)
                if(@essencial_chunk_pair[ind_top.to_i].include?(i)) # essencial dependency
                    essenciallist << i 
                else # not essencial
                    linkedlist << i 
                end 
            end
        } 

        # find maximal density subtree of nodes which has dependency link to this node.
        # (only essencial link)
        essenciallist.each{|es|
            essencial_array << get_md_subtree_es(es, rscore, weight_dict, overlap_hash, overlap_score, threshold, sentid, max_weight, factor, used_words) 
        }

        essencial_array.each{|da| #merge essencial node
            terms_tmp = Hash.new{|hash,key| hash[key]=nil}   
            da.each{|key,val| # for each maximal density subtree
                weight  = key[0]
                dflags  = key[1] 
                score   = val[0] 
                history = val[1] 
                covdpairs = val[2] 

                terms.each{|tkey,tval|# combinations
                    discount = 0.0
                    new_flag = tkey[1].dup 
                    tkey[1].each_with_index{|tfl, tfli| # check overlap
                        if ( dflags[tfli]==true && tfl==true ) # remove overlapped score. 
                            discount += overlap_score[tfli] **($OPT_r).to_f
                            new_flag[tfli]= (dflags[tfli] or tfl) 
                        end 
                        new_flag[tfli]= (dflags[tfli] or tfl) 
                    }  
                    (covdpairs & tval[2]).each{|pairname| discount += rscore[pairname]}

                    new_key   = [weight + tkey[0], new_flag]
                    new_score = [score+tval[0]-discount, (tval[1]+history).sort.uniq, (covdpairs+tval[2]).sort.uniq ]

                    if ( ( new_key[0] < max_weight)&& # 最大サイズを超えているのはダメ
                        !( terms_tmp.has_key?([weight+tkey[0], Array.new(overlap_hash.size,0)]) &&
                          terms_tmp[[weight+tkey[0], Array.new(overlap_hash.size,0)]][0] > new_score[0] ) &&
                          ( !terms_tmp.has_key?(new_key) || terms_tmp[new_key][0] < new_score[0] )) #すでに無ければもしくは前よりもスコア高ければ

                        terms_tmp[new_key] = new_score 
                    end 
                }  
            }    
            # merge 
            terms = terms_tmp.dup 
        }    

        return terms if(terms.size == 0)

        # get mds for un-essencial nodes
        linkedlist.each{|linked|
            dens_array << get_md_subtree_es(linked, rscore, weight_dict, overlap_hash, overlap_score, threshold, sentid, max_weight, factor, used_words) 
        }   

        terms_tmp = Hash.new{| hash,key| hash[key]=nil}   

        dens_array.each{|da|#かかり元ごとに
            da.each{|key,val|#各要素の組み合わせ
                weight  = key[0]
                dflags  = key[1] 
                score   = val[0] 
                history = val[1] 
                covdpairs = val[2] 
                # next if(((1.0*score)/weight) < threshold )

                terms.each{|tkey,tval|
                    discount = 0.0
                    new_flag = tkey[1].dup 
                    tkey[1].each_with_index{|tfl, tfli| #被ってるかどうかチェック
                        if ( dflags[tfli]==true && tfl==true ) #両方1なら被っているのでその分のスコアを引く
                            discount += overlap_score[tfli] **($OPT_r).to_f
                        end 
                        new_flag[tfli]= (dflags[tfli] or tfl) 
                    }  
                    (covdpairs & tval[2]).each{|pairname| discount += rscore[pairname]} 

                    new_key   = [weight + tkey[0], new_flag]
                    new_score = [score+tval[0]-discount, (tval[1]+history).sort.uniq, (covdpairs+tval[2]).sort.uniq ]

                    # スキップのチェック(遅くなったら, ここをきっちりチェックすれば対応減ります)
                    # flag無しがすでにあって, かついまのスコアより高かったら取りやめ
                    if ( (new_score[0] > 0)&& # スコア0なら引き継がない
                        ( new_key[0] < max_weight)&& # 最大サイズを超えているのはダメ
                        !( terms_tmp.has_key?([weight+tkey[0], Array.new(overlap_hash.size,0)]) &&
                          terms_tmp[[weight+tkey[0], Array.new(overlap_hash.size,0)]][0] > new_score[0] ) &&
                          ( !terms_tmp.has_key?(new_key) || terms_tmp[new_key][0] < new_score[0] )) # すでに無ければもしくは前よりもスコア高ければ

                        terms_tmp[new_key] = new_score
                    end 
                }  
            }    

            # add 
            terms_tmp.each{|key,val| 
                if( !terms.has_key?(key) || terms[key][0] < val[0] ) 
                    terms[key] = val
                end 
            }  
            terms_tmp = Hash.new{|hash,key| hash[key]=nil} 
        }    

        return terms 
    end #}}}

    # get maximal density subtree from the sentence
    def get_maximal_density_tree( rscore, weight_dict, threshold, sentid, max_weight, factor, used_words)#{{{

        blist = (0...chunksf.size()).collect{|x|x}
        count = Hash.new{|hash,key| hash[key]=0}  
        total_score = 0.0 
        total_weight = 0.0  
        sentence_weight = 0.0 
        overlap = Hash.new
        overlap_score = Hash.new
        weight_check = false 
        count_size = 1.0 
        constant_factor = factor 
        sentence_factor = constant_factor 

        blist.each{|bi| 
            chunksf[bi].each{|tk| 
                sentence_weight += tk[0].jsize 
            } 
        } 
        blist.each{|bi| 
            next if (weight_dict["w_#{sentid}_#{bi}"] == 0)
            chunksf[bi].each{|tk| # count overlapped words
                if ((tk[1]=~/^名詞/||tk[1]=~/^動詞/||tk[1]=~/^形容/||tk[1]=~/^副詞/) && rscore[tk[2]]>0 )
                    count[tk[2]]+=1 
                end 
            }  
            count.each{|key,val| 
                if(val > 1 && !overlap.has_key?(key))
                    overlap[key] = overlap.size
                    overlap_score[overlap[key]] = rscore[key] #**($OPT_r).to_f
                end 
            } 
        } 

        count.each{|key,val|
            count_size *= val  if(val > 1)
        }   
        sentence_base_score = 0.0 

        sentence_factor = constant_factor 

        # fail if root node is longer then summary limit.
        weight_check = (weight_dict["w_#{sentid}_#{blist.size() -1}"] > max_weight) 
        return [[ -1, 0, 0], sentid, [], []] if(weight_check)

        if($OPT_nocomp || count_size > $OPT_timeout.to_i)# give up compression #{{{
            if( count_size > $OPT_timeout.to_i )
                STDERR.puts "overlap #{sentid}:#{overlap.size} #{count_size}" 
            end 
            sentence_score = 0.0 
            uwords = [] 
            upairs = []

            if(weight_check || sentence_weight == 0|| sentence_weight > max_weight) # check sentence length
                return [[ -1, 0, 0], sentid, [], []] 
            end 

            blist.each{|ind_top|
                wordset = [] 
                chunksf[ind_top].each{|tk|
                    if ((tk[1]=~/^名詞/||tk[1]=~/^動詞/||tk[1]=~/^形容/||tk[1]=~/^副詞/) && rscore[tk[2]]>0 )
                        uwords << tk[2] if(!used_words.include?(tk[2])) # skip
                        wordset << tk[2] 
                    end 
                }

            }  
            uwords.sort.uniq.each{|base|
                sentence_score += rscore[base]**($OPT_r.to_f) if(rscore[base]>0)
            }   
            used_pairs = []

            total_dens = (1.0*sentence_score)/(sentence_weight**($OPT_p.to_f))
            return [[total_dens, sentence_score, sentence_weight], sentid, blist, used_pairs]
        end #}}}


        # Threshold
        # threshold = ( (1.0*total_score) / (total_weight**($OPT_p.to_f)) > 1.0*threshold)?(1.0*total_score)/(total_weight**($OPT_p.to_f)) : threshold
        threshold = 0
        terms = get_md_subtree_es(chunksf.size()-1, rscore, weight_dict, overlap, overlap_score, threshold, sentid, max_weight, sentence_factor, used_words)

        # revoke added factor that is added in subtree
        base_discount = 0.0
        base_discount = sentence_factor * weight_dict["w_#{sentid}_#{chunksf.size()-1}"]  if(weight_dict["w_#{sentid}_#{chunksf.size()-1}"] > 0)#if root exists

        # find the densest one.
        maximum_dens = -1
        maximum_score  = 0.0 
        maximum_length = 0.0 
        maximum_hist   = []
        maximum_pairs   = []
        terms.each{|key,val|
            td = (1.0*val[0] - base_discount)/(key[0]**($OPT_p.to_f)) 
            if(td > maximum_dens && val[0]!=0) # update
                maximum_dens = td 
                maximum_score = val[0] - base_discount 
                maximum_length= key[0] 
                maximum_hist = val[1] 
                maximum_pairs = val[2] 
            end
        }  
        relstr = "" 
        maximum_hist.each{|mhi|
            chunksf[mhi].each{|chi|
                relstr += chi[0] 
            }
        }
        return [[maximum_dens ,maximum_score, maximum_length], sentid, maximum_hist, maximum_pairs ]
    end#}}}
end #}}}
