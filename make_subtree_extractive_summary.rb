#!/usr/bin/ruby

# This script makes a summary from parsed Japanese sentences. 
# Its input requires to be parsed in KNP(patched version). 
# Since this scripts process Japanese sentences and some comments 
# are writen in Japanese, its character codes should be utf-8.
#
# Options:
#  -q filename,    * set query file
#  -f factor  ,      set factor for tree score (default 0.5)
#  -g         ,      early stop
#  -r         ,      qsb scaling factor (default 1.0).
#  -y factor  ,      decay  factor (default 0.2)
#  -p factor  ,      use distorted density. 
#                    the factor introduced by Hui Ling (default 0.2)
#  --nocomp   ,      does not compress sentences
#  --timeout=d,      If the number of combinations of overlapped words in the sentence is larger the number, 
#                    we give up compress the sentence. (default 2500)
#  --direct   ,      use only direct dependencies. 
#
# Requirement: 
#   This code is written for ruby 1.8.
#   It requres some modules to run: 
#       jcode
#       iconv
#       getopts
#   These modules can be downloaded by rubygems if they don't exist.
#   (e.g. $ gems install jcode)
# Author: Hajime Morita

# Modules
require 'jcode' 
require 'iconv' 
require 'getopts'
require 'tool.rb'
require 'Sentence.rb'

# Global settings
# Settings for character set converter.
$Ico_ute  = Iconv.new('EUC-JP', 'UTF-8') 
$Ico_etu  = Iconv.new('UTF-8',  'EUC-JP') 
# Settings for character set used in ruby.
$KCODE ="UTF8"

# Maximal length of summary
Max_weight = 140

# Querysnowball
def relational_score(query, coocdist, coocfreq, cooc_pair, threshold) #{{{
    rscore = Hash.new(0)
    r1 = Hash.new(0)
    r2 = Hash.new(0)
    query_sum = 0.0
    r1_sum = 0.0
    r2_sum = 0.0
    query.each{|base,value|
        rscore[base] = value
        query_sum += value
    }    
    query.sort.uniq.each{|base,value|
        cooc_pair[base].sort.uniq.each{|wd, wval| #[[0,"word"],idf]
            if !query.key?(wd)
                r1[wd] = wval
                key1, key2 = [base,wd].sort{|x,y| -(x<=>y)}
                key = [key1,key2]
                rscore[wd] += wval*(rscore[base]/query_sum)*(coocfreq[key]/(coocdist[key]+1.0))
            end
        }  
    }    
    r1.each{|base,value|
        r1_sum += rscore[base]
    }    
    r1.each{|base,value|
        cooc_pair[base].each{|wd, wval| #[[0,"word"],idf]
            if !r1.key?(wd) && !query.key?(wd)
                r2[wd] = wval
                key1, key2 = [base,wd].sort{|x,y| -(x<=>y)}
                key = [key1,key2]
                rscore[wd] += wval*(rscore[base]/r1_sum)*(coocfreq[key]/(coocdist[key]+1.0))
            end
        }  
    }    
    r2.each{|base,value|
        r2_sum += rscore[base] 
    }    
    [r1,r2,rscore]
end #}}}   

# Options
def print_usage#{{{
    $stderr.print <<HELP
Usage: ruby make_subtree_extractive_summary.rb [options] 
  -q filename,    * set query file
  -f factor  ,      set factor for tree score (default 0.5)
  -g         ,      early stop
  -r         ,      qsb scaling factor (default 1.0).
  -y factor  ,      decay  factor (default 0.2)
  -p factor  ,      use distorted density. 
                    the factor introduced by Hui Ling (default 0.2)
  --nocomp   ,      does not compress sentences
  --timeout=d,      If the number of combinations of overlapped words in the sentence is larger the number, 
                    we give up compress the sentence. (default 2500)
  --direct   ,      use only direct dependencies. 
HELP
end#}}}

if !getopts("help", "g", "d:1.0", "q:", "r:1.0", "p:1.0", "f:1.0", "m", "y:0.2", "direct", "nocomp", "timeout:2500" ) #{{{
    $stderr.puts("illegal option")
    print_usage
    exit(2)
end#}}}


# 新しく文を選択するペナルティをつけるための, ゲタ
# A factor for penalty of selecting new sentences. 
Constant_factor = ($OPT_f).to_f 

# A parameter used for balansing between score and costs wheile summary extraction. 
# We mentioned this parameter as $r$ in our paper (Morita et.al. 2013). 
$Hui_depth = $OPT_p.to_f


# Check input file before loading large hash file.
if !(File.exist?($OPT_q.to_s))
    STDERR.puts "can't access queries #{$OPT_q.to_s}."
    exit(2) 
end  

# "juman.hash" contains df of unigrams.
# The df was calculated on Mainichi newspapers. 
# gugram is named by historical reason.
$gugram = Marshal.load(File.new("juman.hash","r"))#UTF/juman/df
$gugram_size = 1554951 #number of documents.

# read input query files
# The file is constructed as follows:
# ----
# query sentence(s) (KNP parsed)
# source sentences (KNP parsed)
# [EOF]
# ----
# The first sentence in the input is interpreted as query.
# Each sentences are delimitterd by EOS.

sourcefile = File.new($OPT_q.to_s)
sourcedoc = sourcefile.read 

# Extract query id from input file name. 
# The id is needed for POURPRE evaluation. 
query_id = ""
if($OPT_q.to_s =~ /ACLIA(\d).*JA-T?(\d*)\..*/)
    query_id = "#{$1}.#{$2}"
else
    query_id = $OPT_q
end

# Read Query (sentence)
raw_sentences = sourcedoc.split(/EOS/)
question  = Sentence.new(raw_sentences.shift.strip)
puts "Query:"+question.to_s 


# Initialize word dictionary (stores dynamic socores for words)
worddict = Hash.new{|hash,key| 
    if $gugram[key] > 0 && ( $gugram_size/ $gugram[key] )>0
        hash[key] = Math.log( $gugram_size/ $gugram[key] )
    else 
        #hash[key] = Math.log( $gugram_size/ 1.0 )# for unknown words
        hash[key] = 0.0
    end 
}  
# co-ocurrence distant
coocdist = Hash.new{|hash,key| hash[key]=1.0/0}
# co-occurrence frequency
coocfreq = Hash.new{|hash,key| hash[key]=0}#count co-occur
# term frequency
termfreq = Hash.new{|hash,key| hash[key]=0}#count term
# set of co-occurred words for a word.
que_pair = Hash.new{|hash,key| hash[key]=[]}

# Read Source sentences
topic_sentences = Array.new#{{{

raw_sentences.each{|sentence|
    sent = Sentence.new(sentence.strip) 
    topic_sentences << sent if(sent.chunksf.size() > 0)#drop sentences who have no content word.
}

# Variables that stores surface of bunsetsu and its position.
variable_surface  = Hash.new() 

#calculate co-occurrence distance, frequency, 
topic_sentences.each_with_index{|sent, sent_i| # for each sentences
    sent.chunksf.each_index{|index| #for each chunks(bunsetsu)
        tks = sent.chunksf[index] 
        tks.each{|tk| termfreq[tk[2]] += 1 if ( tk[1]=~/^名詞/||tk[1]=~/^動詞/||tk[1]=~/^形容/||tk[1]=~/^副詞/) } 
        (0...index).each{|index2| # for each pair of bunsetsu
            # when the two bunsetsu have a dependency link.
            if sent.distance_matrix_c[index][index2]>=0 || sent.distance_matrix_c[index2][index]>=0
                #sort
                key1, key2 = [sent.chunksf[index],sent.chunksf[index2]].sort{|x,y| -(x<=>y)}

                # extract content words
                jword = [] # a list of content word
                (key1+key2).each{|k|
                    tokenpos = k[1] 
                    # skip not in (noun verv adj adv)
                    next if !(tokenpos=~/^名詞/||tokenpos=~/^動詞/||tokenpos=~/^形容/||tokenpos=~/^副詞/) 
                    jword <<  k[2] #get surface of the word.
                } 
                jword.each_other{|jw1,jw2| # for each co-occurred word
                    key = [jw1,jw2].sort{|x,y| -(x<=>y)} 
                    coocdist[key] = [coocdist[key], sent.distance_matrix_c[index][index2], sent.distance_matrix_c[index2][index]].min
                    coocfreq[key] += 1
                    que_pair[jw1] += [[jw2, worddict[jw2]]]
                    que_pair[jw2] += [[jw1, worddict[jw1]]]
                } 
            end 
        } 
    }
}

#remove overalp
que_pair.keys.each{|key|
    que_pair[key].sort!
    que_pair[key].uniq!
}#}}}

###############################################
# Querysnowball
###############################################
# get relational words and score
query    = question.jiritsu.collect{|w| [w, worddict[w]] }
query_score = Hash.new 
query.each{|base,value| query_score[base] = value }
r1_word, r2_word, rscore = relational_score(query_score, coocdist, coocfreq, que_pair, 0)
rwords = r1_word.merge query_score
r2words = r2_word.merge rwords


###############################################
# Initialize cost of bunsetsu
###############################################
# A cost of an existing word (directly varies depends on summarizing process).
weight_dict = Hash.new(0)

# translate sentences into a set of a pair of chunand its cost. 
topic_sentences.each_with_index{|sent, is|
    sent.chunksf.each_with_index{|wd, ind|
        variable_surface["c_#{is}_#{ind}"] = wd.collect{|w| w[0]}.join("")

        weight = 0 
        sent.chunksf[ind].each_with_index{|tk, tk_ind| 
            weight += tk[0].jsize # weight
            score_tmp = 0 
            score_tmp = rscore[tk[2]] if (tk[1]=~/^名詞/||tk[1]=~/^動詞/||tk[1]=~/^形容/||tk[1]=~/^副詞/) #content words only
        }  
        weight_dict["w_#{is}_#{ind}"] = weight #entry to weight dictionary
    }  
}

###############################################
# Summarization
###############################################

# その時点での圧縮された部分木の集合
# Compressed (extracted) subtrees (not equal summary).  
# This is (mostly) corresponding to a set of subt in Algorithm 2 in the paper. 
comp_results = Array.new #[[[dens,score,length], index of the sentence, [chunks], updatetime], [[dens,score,length], index of the sentence, [chunks], updatetime], ...] 

# 文ごとの使用済み単語を記録.同じ文では同じ単語はスコアを与えない
# A log for used words. Because we don't give any score to overlapped word in the same sentnce.
sentence_used_words = Hash.new{|hash,key| hash[key]=Array.new} 

# 更新された単語をイテレーションごとに記録しておく.  
# stores words added to the summary for each iteration. 
updated_words = []

# Initial compression
# We need to compress all sentenfces in the first iteration.
# From the second compression, we can skip needless compression. 
topic_sentences.each_with_index{|sent, is| 
    comp_results << (sent.get_maximal_density_tree(rscore, weight_dict, 0, is, Max_weight, Constant_factor, sentence_used_words[is])+[0]) #末尾は更新時間 
} 

# sort by its density.
comp_results.sort!{|x,y| -(x[0][0]<=> y[0][0])} 

# summary
summary_item = Hash.new{|key,val| Array.new} 
summary_size = 0 
summary_score = 0 
itr = 0 

# Main loop for summarization
while( summary_size < Max_weight )
    # early stop to avoid to add a crushing compressed sentence in the last few charactes.
    break if (($OPT_g) && (summary_size > Max_weight*0.95)) 

    loop_count = 0 #for debug printing
    STDERR.print "#itr: #{itr} prior loop:#{loop_count}"

    # the densest subtree
    top_comp = nil

    while(comp_results.size > 0) 
        STDERR.print "\r#itr: #{itr} prior loop:#{loop_count}/#{comp_results.size}"
        STDERR.flush 

        # もっともスコアの(上限が)高い部分木 
        # A candidate of the densest subtree. 
        top_comp = comp_results.shift 

        # 部分木の最終更新時間から,更新された単語集合を選ぶ. 
        # get a set of words added to summary while from the timestamp to now
        difference = updated_words[top_comp[4]...itr].flatten.sort.uniq 

        # 部分木が含む単語の集合を求める
        # get a set of words included in the subtree.
        used_words = top_comp[2].collect{|tki| 
            topic_sentences[top_comp[1]].chunksf[tki].collect{|tk|tk[2]} #base 
        }.flatten.sort.uniq 

        # すでに被覆された単語と同じ単語は除く
        # sentence_used_wordsは実際にその文から要約に組み込まれた単語の集合なので 
        # 実際に部分木のスコアに関わっている単語集合を選ぶ.  
        #
        # Remove words that have same bases with words extracted from the sentence. 
        # (Those words must not have any scores.)
        used_words = used_words - sentence_used_words[top_comp[1]] 

        # 文字数制約を満たしているかどうか, すでに更新された単語を含んでいないか
        # Check the length satify length limit, and not contain updated words. 
        if ((top_comp[0][2] <= (Max_weight - summary_size) &&        # length limit
             (used_words & difference).size == 0 && top_comp[0][0]>0 ))# not contains updated words
            break # The subtree does not need to re-compress.
        end 

        # Re-compression
        # 要約に組み込まない場合は一番上を圧縮しなおし
        # 文字数制限にあう圧縮が存在しない場合には密度マイナスを返すので無視される
        # When there is no candidates that satify length limit, it returns minus value. 
        if(top_comp[0][0] > 0) # drop the sentence
            comp_results << ( (topic_sentences[top_comp[1]].get_maximal_density_tree(rscore, weight_dict, 0, top_comp[1], 
                                                                                     Max_weight-summary_size, Constant_factor,sentence_used_words[top_comp[1]]))+[itr])
            comp_results.sort!{|x,y| -(x[0][0]<=>y[0][0])} #sort again
        end 

        loop_count +=1
    end   
    STDERR.puts ""
    break if(top_comp == nil) # we couldn't get the densest subtree (= no more sentences in the source)

    # add top_comp to the summary
    if(top_comp[0][0]>0)
        length=0.0
        scores=0.0 
        used_words =  []
        surface = "" 

        # Revoke constant penalty when the summary already includes its root. 
        scores += ($OPT_f.to_f) if weight_dict["w_#{top_comp[1]}_#{0}"]==0 
        top_comp[2].each{|tki|

            # skip covered woreds
            next if weight_dict["w_#{top_comp[1]}_#{tki}"]==0

            # update cost 
            length += weight_dict["w_#{top_comp[1]}_#{tki}"] # update summary cost
            weight_dict["w_#{top_comp[1]}_#{tki}"]=0 # update cost of the covered bunsetsu

            # add to summary
            summary_item[top_comp[1]] += [tki] 

            # calculate score of extracted subtree, and update used_words
            topic_sentences[top_comp[1]].chunksf[tki].each{|tk| #for each bunsetsu
                surface += tk[0] # extract surface to realize compressed sentence (for temporal output)
                if(rscore[tk[2]]>0) 
                    scores += rscore[tk[2]]**($OPT_r.to_f) # scaling factor(default:1.0)
                    used_words << tk[2] # collect used words (base words)
                end 
            }  
        }  
        puts "# length:#{length},t_w:#{top_comp[0][2]} scores:#{scores + (length-1)*($OPT_f.to_f)}, t_s:#{top_comp[0][1]}" 

        used_words = used_words.flatten.sort.uniq 
        used_words = used_words - sentence_used_words[top_comp[1]] # remove already used words.

        # recalculate rscore
        used_words.each{|id| 
            if( $OPT_y && $OPT_y.to_f > 0) 
                rscore[id] *= $OPT_y.to_f # damping rate in Eq.(4) in our paper.
            else 
                rscore[id]=0
            end 
        }

        # update updated_words
        updated_words << used_words # used words in this iteration
        sentence_used_words[top_comp[1]] = (sentence_used_words[top_comp[1]]+used_words).sort.uniq # update used words in the sentence.

        # update summary length
        summary_size += length
        # update summary score
        summary_score += scores

        # re-compress the sentence
        comp_results << (topic_sentences[top_comp[1]].get_maximal_density_tree(rscore, weight_dict, 0, top_comp[1], 
                                                                               Max_weight-summary_size, Constant_factor,sentence_used_words[top_comp[1]])+[itr])
        comp_results.sort!{|x,y| -(x[0][0]<=>y[0][0])} 
    end 
    itr += 1 
end  

# output
count = 1
summary_base = Array.new 
sumlength = summary_item.collect{|key,val|
    string = val.sort{|x,y| (x <=> y)}.uniq.collect{|x| 
        variable_surface["c_#{key}_#{x}"].strip
    }.join("")

    summary_base << "#{query_id} firsttry #{count} #{key}"
    summary_base << "%#{query_id} compsent:#{string}"
    summary_base << "%#{query_id}  rawsent:#{(topic_sentences[key].to_s)}"
    count += 1 
    puts (string)
    string 
}.join("").sub(/\n/,"").jsize

puts
puts "p /#{sumlength}"
puts "r /" 
puts

summary_base.each{|summary_sent|
    puts summary_sent 
}


